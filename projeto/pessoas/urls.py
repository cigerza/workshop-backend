from django.urls import path
from .views import listar_pessoas

urlpatterns = [
    path('pessoas/', listar_pessoas)
]
